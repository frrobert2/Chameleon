# Chameleon
A simple Conky Script that mimics the i3wm status bar.
The script's purpose is  to add a conky bar at the bottom of the screen that sits on top of i3staus bar and mimics the i3status bar with the appearance of a two line i3staus bar.  The script is designed for for i3wm users that want to run i3 status but also want to run conky.  An example would be a user that uses the i3 status bar to hold the system icons.                                
                                                                              
The user can change the alignment if they want to reposition the bar. 

The conkyrc file uses sensors to measure cpu tempature.  Sensors can be found in most repostitories.  

The script also uses a small bash script to retrive your external ip.
